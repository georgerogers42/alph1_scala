package alph1;

import scala.math.Integral
import java.util.regex.Pattern
import java.io.{Writer, BufferedReader}

object Alph1 {
  def capitalize(word: String) : String = {
    if(word == "") {
      word
    } else {
      word.substring(0, 1).toUpperCase() + word.substring(1)
    }
  }

  private val wordSplit = Pattern.compile("\\W+")
  def buildTable[A](table: Map[String, A], r: BufferedReader)(implicit N : Numeric[A]) : Map[String, A] = {
    import N._
    var tbl = table
    while(true) {
      val line = r.readLine()
      if(line == null) {
        return tbl
      }
      for(iword <- wordSplit split(line)) {
        val word = capitalize(iword)
        tbl += ((word, tbl.getOrElse(word, fromInt(0)) + fromInt(1)))
      }
    }
    return tbl
  }

  def report[K, V](w: Writer, table: Iterable[(K, V)], o: ((K, V), (K, V)) => Boolean) {
    for(pair <- table.toList.sortWith(o)) {
      val (k, v) = pair
      w.write(f"$k%24s: $v%10s\n")
      w.flush()
    }
  }

  def byFirst[K, V](a: (K, V), b: (K, V))(implicit X: Ordering[K]) : Boolean = {
    import X._
    a._1 < b._1
  }

  def bySecond[K, V](a: (K, V), b: (K, V))(implicit X: Ordering[V]) : Boolean = {
    import X._
    a._2 < b._2
  }
}

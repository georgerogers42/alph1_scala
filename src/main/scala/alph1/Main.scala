package alph1;
import java.io.{BufferedReader, FileReader, OutputStreamWriter}

object Main {
  type Count = Long
  def main(args: Array[String]) {
    val o : ((String, Count), (String, Count)) => Boolean =
      if(args.length > 0 && args(0) == "-a") {
        Alph1.byFirst _
      } else {
        Alph1.bySecond _
      }
    val w = new OutputStreamWriter(System.out)
    for(i <- 1 to 100) {
      w.write(f"# Begin Iteration: $i%d\n")
      w.flush()
      val f = new FileReader("TEXT-PCE-127.txt");
      val table =
        try {
          val r = new BufferedReader(f);
          Alph1.buildTable(Map[String, Count](), r)
        } finally {
          f.close()
        }
      Alph1.report(w, table, o)
      w.write(f"# End Iteration: $i%d\n")
      w.flush()
    }
  }
}
